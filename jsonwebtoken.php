<?php  
$header = json_encode(['typ' => 'JWT', 'alg' => 'HS256']);
$base64UrlHeader = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));
 $payload = json_encode([
 "iss" => "education.com.br",
 "exp" => 1300819380,
 "name" => "",
 "admin" => true
]);
$base64UrlPayload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($payload));
$secret = 'minha-chave'; 
$signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, $secret, true);
$base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));
$jwt = $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;
 echo $jwt
?>