<?php
class Usuario{
	private $id;
	private $nome;
	private $telefone;
	private $email;
	private $data;
	private $genero;
	

    private function __construct(){

    }
    private function __destruct(){

    }
    public function getNome(){
		return $this->nome;
	}
	public function setNome($n){
		$this->nome = (isset($n)) ? $n : NULL;
	}
	public function getTelefone(){
		return $this->telefone;
	}
	public function setTelefone($i){
		$this->telefone= (isset($i)) ? $i : NULL;
	}
	public function getEmail(){
		return $this->email;
	}
	public function setEmail($c){
		$this->email= (isset($c)) ? $c : NULL;
	}
	public function getData(){
		return $this->data;
	}
	public function setData($c){
		$this->data= (isset($c)) ? $c : NULL;
	}
	public function getGenero(){
		return $this->genero;
	}
	public function setGenero($c){
		$this->genero= (isset($c)) ? $c : NULL;
	}
	public function getId(){
		return $this->id;
	}
	public function setId($c){
		$this->id= (isset($c)) ? $c : NULL;
	}
	
}
?>